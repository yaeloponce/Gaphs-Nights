﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OnActionDissapearUI))]
public class OnActionDissapearUIEditor: Editor
{
    override public void OnInspectorGUI()
    {
        OnActionDissapearUI script = (OnActionDissapearUI)this.target;
        
        script.action = (OnActionDissapearUI.Action) EditorGUILayout.EnumPopup("Input Action", script.action);
        if(script.action == OnActionDissapearUI.Action.PressKey)
        {
            //EditorGUILayout.PrefixLabel("Keyboard Key");
            script.KeyboardKey = EditorGUILayout.TextField("Keyboard Key",script.KeyboardKey);
        }
        //DrawDefaultInspector();
    }
}
