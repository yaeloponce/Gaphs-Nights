﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathShower : MonoBehaviour {
    private GameObject[] paths;
    // Use this for initialization
    void Start()
    {
        paths = GameObject.FindGameObjectsWithTag("path");
        foreach (var obj in paths)
        {
            obj.GetComponent<CanvasRenderer>().SetAlpha(0);
        }
    }
    public void turnOffAllPaths()
    {
        foreach(var obj in paths)
        {
            obj.GetComponent<CanvasRenderer>().SetAlpha(0);
        }
    }
	public void hightlightPath(int actual)
    {
        //print("entra, actual: " + actual);
        int[] parents = GameObject.FindObjectOfType<DijkstraManager>().getParents();
        int source, dest=actual;
        string[] name;
        do
        {
            source = parents[dest];
            //print("source: " + source);
            //print("dest: " + dest);
            for (int i = 0; i < paths.Length; i++)
            {
                name = paths[i].name.Split('_');    //name structure: path_SOURCE_DEST
                if(source == System.Convert.ToInt32(name[1]) && dest == System.Convert.ToInt32(name[2]) ||
                    dest == System.Convert.ToInt32(name[1]) && source == System.Convert.ToInt32(name[2]))
                {
                    paths[i].GetComponent<CanvasRenderer>().SetAlpha(0.5f);
                }
            }
            dest = source;
        } while (source != 0);
    }
}
