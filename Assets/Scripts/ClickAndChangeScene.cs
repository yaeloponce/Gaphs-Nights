﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ClickAndChangeScene : MonoBehaviour
{

    private Button goButton;
    public string SceneToGo;
    public AudioClip ButtonSound;

    void OnEnable()
    {
        goButton = this.gameObject.GetComponent<Button>();
        //LevelManager lvlManager = GameObject.FindObjectOfType<LevelManager>();
        goButton.onClick.AddListener(()=>Wrapper());
    }
    public void Wrapper()
    {
        AudioSource.PlayClipAtPoint(ButtonSound,Camera.main.transform.position);
        LevelManager lvlManager = GameObject.FindObjectOfType<LevelManager>();
        StartCoroutine(lvlManager.LoadScene(SceneToGo));
        //lvlManager.LoadScene(SceneToGo);
    }
    void OnDisable()
    {
        goButton.onClick.RemoveAllListeners();
    }
}