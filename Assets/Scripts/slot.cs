﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class slot : MonoBehaviour, IDropHandler
{
    private Text dialogueText;
    AudioSource AudioSource;
    public AudioClip WrongAnswer;
    public AudioClip RightAnswer;

    public void Start()
    {
        dialogueText = GameObject.Find("DialogueText").GetComponent<Text>();
        AudioSource = GameObject.Find("SFX").GetComponent<AudioSource>();
        AudioSource.volume = 1.0f;
    }
    public void OnDrop(PointerEventData eventData)
    {
        int entrante, actual, cont = 0;
        DijkstraManager dijkstraManager = FindObjectOfType<DijkstraManager>();
        AudioSource.volume = 1.0f;
        if (transform.CompareTag("pv") && DragHandler.itemBeingDragged.tag=="node")
        {
            foreach (DijkstraManager.DijkstraGameTable obj in dijkstraManager.getTableElements())
            {
                if (obj.objectPv != null && obj.objectPv.name == transform.name)
                {
                    break;
                }
                cont++;
            }
            entrante = Convert.ToInt32(DragHandler.itemBeingDragged.GetComponent<Text>().text);
            if (entrante == dijkstraManager.getParents()[cont])
            {
                dijkstraManager.getTableElements()[cont].parent = entrante;
                transform.GetComponent<Text>().text = entrante+"";
                dialogueText.text = "Maravilloso, sigue así.";
                AudioSource.volume = 0.20f;
                AudioSource.PlayOneShot(RightAnswer);
            }
            else
            {
                print("El nodo anterior es otro");
                dialogueText.text = "Observa con cautela, el nodo anterior es otro.";
                AudioSource.PlayOneShot(WrongAnswer);
            }
            
        }
        if(transform.name == "buttonSumText" && DragHandler.itemBeingDragged.CompareTag("edge") && DragHandler.itemBeingDragged.name!="infinity")
        {   //si es del botón donde se hacen la sumas, el item arrastrado es un edge y no es infinito, entra
            int resultado;
            entrante = Convert.ToInt32(DragHandler.itemBeingDragged.GetComponent<Text>().text);
            //agregar el caso
            actual = Convert.ToInt32(transform.GetComponent<Text>().text);
            resultado = actual + entrante;
            transform.GetComponent<Text>().text = resultado+"";
        }
        if (transform.CompareTag("dv") && DragHandler.itemBeingDragged.CompareTag("edge"))
        {
            foreach (DijkstraManager.DijkstraGameTable obj in dijkstraManager.getTableElements())
            {
                if (obj.objectDv != null && obj.objectDv.name == transform.name)
                {
                    break;
                }
                cont++;
            }
            if (DragHandler.itemBeingDragged.name != "infinity")
            {
                entrante = Convert.ToInt32(DragHandler.itemBeingDragged.GetComponent<Text>().text);

                if (entrante == dijkstraManager.getDistances()[cont]) // Hacer Si concuerda con el valor calculado
                {
                    dijkstraManager.getTableElements()[cont].distance = entrante;
                    transform.GetComponent<Text>().text = entrante + "";
                    dialogueText.text = "Excelente, continúa así.";
                    AudioSource.volume = 0.20f;
                    AudioSource.PlayOneShot(RightAnswer);
                }
                else //Si no concuerda, no permitir movimiento Y mandar mensaje de que no concuerda
                {
                    print(entrante+" vs "+ dijkstraManager.getDistances()[cont]);
                    //print("Falta mejorar movimiento");
                    if (dijkstraManager.getDistances()[cont] == int.MaxValue){
                        dialogueText.text = "Pon atención a las conexiones, en este caso no existe, así que haz de decir que su distancia es infinita.";
                        AudioSource.PlayOneShot(WrongAnswer);
                    }
                    else
                    {
                        dialogueText.text = "Estudia las demás posibilidades, el camino indicado es otro.";
                        AudioSource.PlayOneShot(WrongAnswer);
                    }
                }
            }
            else
            {
                if(dijkstraManager.getDistances()[cont] == int.MaxValue)
                {
                    transform.GetComponent<Text>().text = DragHandler.itemBeingDragged.GetComponent<Text>().text;
                    dijkstraManager.getTableElements()[cont].distance = Int32.MaxValue;
                    dialogueText.text = "Muy bien, prosigue.";
                    AudioSource.volume = 0.20f;
                    AudioSource.PlayOneShot(RightAnswer);
                }
                else
                {
                    print("Si existe un camino");
                    dialogueText.text = "El infinito indica que hay una distancia infinta entre ellos, es decir, no se conectan, pero entre esos dos nodos si existe un camino, analiza detenidamente.";
                    AudioSource.PlayOneShot(WrongAnswer);
                }
            }
        }
        if(transform.CompareTag("pv") && DragHandler.itemBeingDragged.tag == "edge")
        {
            print("Los lados van en la columna de d[v]");
            dialogueText.text = "Los lados van en la columna de d[v].";
            AudioSource.PlayOneShot(WrongAnswer);
        }
        if (transform.CompareTag("dv") && DragHandler.itemBeingDragged.tag == "node")
        {
            print("Los nodos van en la columna de p[v]");
            dialogueText.text = "Los nodos van en la columna de p[v].";
            AudioSource.PlayOneShot(WrongAnswer);
        }
        dijkstraManager.isStepOver();
    }
    
}
