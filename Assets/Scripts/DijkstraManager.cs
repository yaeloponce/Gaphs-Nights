﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class DijkstraManager : MonoBehaviour{
    public struct DijkstraGameTable
    {
        public int distance;
        public int parent;
        public GameObject objectDv;
        public GameObject objectPv;
        public DijkstraGameTable(GameObject gameDistance, int distanceVal, GameObject gameParent, int parentVal) {
            distance = distanceVal;
            objectDv = gameDistance;
            parent = parentVal;
            objectPv = gameParent;
        }
        public string printString()
        {
            return objectDv.name + ", "+distance;
        }
    }
    private int minDist, u=-1;
    private int[,] AdjacentMatrix;
    private int[] dist;
    private int[] parent;
    private List<int> VminS;
    private DijkstraGameTable[] dijkstraTable;
    public TextAsset file;

    // Use this for initialization
    void Start()
    {
        Read_FillFromArchive archivo = gameObject.AddComponent<Read_FillFromArchive>();
        
        AdjacentMatrix = archivo.fillMatrix(file);

        VminS = new List<int>();
        //dijkstraTable = new DijkstraGameTable[GameObject.FindGameObjectsWithTag("dv").Length];
        dijkstraTable = new DijkstraGameTable[ AdjacentMatrix.GetLength(0)];
        dijkstraTable[0].objectDv = null;
        dijkstraTable[0].objectPv = null;
        dijkstraTable[0].parent = -1;
        for (int i = 1; i < dijkstraTable.Length; i++)
        {
            //print(i);
            dijkstraTable[i].objectDv = GameObject.Find("dv"+(i));
            dijkstraTable[i].distance = 0;
            dijkstraTable[i].objectPv = GameObject.Find("pv"+(i));
            dijkstraTable[i].parent = -1;
            
            dijkstraTable[i].objectDv.GetComponent<Text>().text = "";
            dijkstraTable[i].objectPv.GetComponent<Text>().text = "-";
            //print(dijkstraTable[i].parent);
        }
        for ( int i = 1; i < dijkstraTable.Length; i++)
        {
            VminS.Add(i);
        }

        dist = new int[AdjacentMatrix.GetLength(0)];
        parent = new int[dist.Length];

        for (int i = 0; i < dist.Length; i++)
        {
            dist[i] = AdjacentMatrix[0, i];
            parent[i] = 0;
            //print(dist[i]+" "+ parent[i]);
        }
       
        dijkstraTable[0].distance = dist[0];
        parent[0] = -1;

        GameObject.Find("node_0").GetComponentInChildren<Image>().color = Color.yellow;
    }

    public DijkstraGameTable[] getTableElements()
    {
        return dijkstraTable;
    }
    public int[] getDistances()
    {
        return dist;
    }
    public int[] getParents()
    {
        return parent;
    }
    public void isStepOver()
    {
        for(int i=1; i < dist.Length; i++)
        {
            //print(dist[i] + " vs "+dijkstraTable[i].distance+" || "+parent[i]+" vs "+ dijkstraTable[i].parent);
            if (dist[i] != dijkstraTable[i].distance || parent[i] !=dijkstraTable[i].parent)
            {
                //print("regresa sin hacer nada");
                return;
            }
        }
        //print("pasa");
        dijkstraStep();

    }
    public void dijkstraStep()
    {
        //print("entra al siguiente paso");
        if (isFinished())
        {
            var lvlManager = GameObject.FindObjectOfType<LevelManager>();
            lvlManager.LoadScene();
            return;
        }
        GameObject.Find("node_0").GetComponentInChildren<Image>().color = Color.grey;
        minDist = int.MaxValue;
        for (int i = 1; i < dist.Length; i++)
        {
            //print(i + ": " + dist[i]+" min "+minDist+" VminS "+VminS.Contains(i+1));
            if (dist[i] < minDist && VminS.Contains(i))
            { // get minimun val in VminS
                minDist = dist[i];
                u = i;
            }
        }
        foreach(var obj in GameObject.FindObjectsOfType<HighlightNodes>())
        {
            //print(obj.name+" update="+u);
            obj.updateActualVertex();
        }
        //GameObject.FindObjectOfType<HighlightNodes>().updateActualVertex(); ;
        VminS.Remove(u); //remove u from VminS and add to S

        //print("u: "+u+1);
        foreach (int v in VminS)
        {
            int weight = AdjacentMatrix[u,v];
            //print(u+"_dist[u] "+dist[u]+"w: "+weight+" dist[i-1] "+dist[v-1]);
            if (weight != int.MaxValue && dist[u] + weight < dist[v])
            {
                dist[v] = dist[u] + weight;
                parent[v] = u;
            }
        }
        //for (int i = 1; i < dist.Length; i++)
        //{
        //    print(dist[i]+" "+ parent[i]);
        //}
        //print("fin del paso");
    }
    public int getActualVertex()
    {
        return u;
    }
    public int getActualDist()
    {
        return dist[u];
    }
    public bool isFinished()
    {
        if(VminS.Count == 0)
            return true;
        return false;
    }
}
