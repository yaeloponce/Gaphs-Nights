﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SumButton : MonoBehaviour {
    public GameObject buttonText;
    private int defVal = 0;
	// Use this for initialization
	void Start () {
		
	}
	public void updateText(int num)
    {
        defVal = num;
        buttonText.GetComponent<Text>().text = num+"";
    }
    public void updateText()
    {
        buttonText.GetComponent<Text>().text = defVal + "";
    }
}
