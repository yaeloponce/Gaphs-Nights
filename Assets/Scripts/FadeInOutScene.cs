﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum InOut
{
    FadeIn, FadeOut
}
public class FadeInOutScene : MonoBehaviour
{

    public Texture2D texture;
    [Range(0.1f, 1f)]
    public float fadeSpeed = 0.2f;
    public int drawDepth = -1000;

    private float alpha = 1f;
    public InOut FadeDirection = InOut.FadeIn;
    private int fadeDir = -1;
    private float initAlpha;
    void Start()
    {
        if (FadeDirection == InOut.FadeOut)
        {
            fadeDir = 1;
            alpha = 0.0f;
        }
        initAlpha = alpha;
    }
    public IEnumerator Fade(InOut dir)
    {
        FadeDirection = dir;
        if(InOut.FadeIn == dir)
        {
            //print("fadeIn");
            fadeDir = -1;
            alpha = 1.0f;
            initAlpha = alpha;
            //yield return new WaitUntil(() => initAlpha == 1 && alpha <= 0);
            yield return 0;
        }
        else
        {
            fadeDir = 1;
            alpha = 0.0f;
            initAlpha = alpha;
            yield return 0;
        }
    }
    public IEnumerator Fade(Texture2D tex, InOut dir)
    {
        texture = tex;
        //print("texture:" + tex.name + " fadeDir:" + fadeDir + " alpha" + alpha + " initAlpha:" + initAlpha);
        if (InOut.FadeIn == dir)
        {
            //print("Fade In");
            fadeDir = -1;
            alpha = 1.0f;
            initAlpha = alpha;
            yield return new WaitUntil(() => initAlpha == 1 && alpha <= 0);
        }
        else
        {
            fadeDir = 1;
            alpha = 0.0f;
            initAlpha = alpha;
            yield return new WaitUntil(() => initAlpha == 0 && alpha >= 1);
        }
    }
    void OnGUI()
    {
        if (initAlpha == 1 && alpha > 0 || initAlpha == 0 && alpha < 1)
        {
            //print("IA:" + initAlpha + " --- A:" + alpha);
            alpha += fadeDir * fadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);

            Color newColor = GUI.color;
            newColor.a = alpha;
            GUI.color = newColor;
            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
        }
    }
    public float GetAlpha()
    {
        return alpha;
    }
    public float GetInitAlpha()
    {
        return initAlpha;
    }
}