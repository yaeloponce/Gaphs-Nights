﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnActionDissapearUI : MonoBehaviour {
    public enum Action
    {
        PressKey, MoveMouse
    }
    //public GameObject Object;
    public Action action;
    public string KeyboardKey;
    private float XAxis;
    private float YAxis;
    public void Start()
    {
        XAxis = Mathf.Abs(Input.GetAxis("Mouse X"));
        YAxis = Mathf.Abs(Input.GetAxis("Mouse Y"));
    }
	void Update () {
		if(action == Action.PressKey && Input.GetKeyUp(KeyboardKey))
        {
            //this.SetActive(false);
            gameObject.SetActive(false);
        }
        else if(action == Action.MoveMouse && (Mathf.Abs(XAxis - Mathf.Abs(Input.GetAxis("Mouse X"))) > 0.5 || Mathf.Abs(YAxis - Mathf.Abs(Input.GetAxis("Mouse Y"))) > 0.5)){
            gameObject.SetActive(false);
        }
	}
}
