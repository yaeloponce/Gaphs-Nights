﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum playerState
{
    normal, talking
}
public class PlayerCharacterController : MonoBehaviour {
    public CharacterController player;
    public float speed;
    public float gravity = 9.81F;
    private float vSpeed = 0;
    private float tSpeed;
    private playerState state = playerState.normal;
	// Use this for initialization
	void Start () {
        player = GetComponent<CharacterController>();
        tSpeed = speed * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (state == playerState.talking)
        {
            return;
        }
        if (player.isGrounded)
        {
            vSpeed = 0;
        }
        vSpeed -= gravity * Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            player.Move(new Vector3(player.transform.forward.x * tSpeed, vSpeed, player.transform.forward.z * tSpeed));
        }
        if (Input.GetKey(KeyCode.S))
        {
            player.Move(new Vector3(-player.transform.forward.x * tSpeed, vSpeed, -player.transform.forward.z * tSpeed));
        }
        if (Input.GetKey(KeyCode.A))
        {
            player.Move(new Vector3(-player.transform.right.x * tSpeed, vSpeed, -player.transform.right.z * tSpeed));
        }
        if (Input.GetKey(KeyCode.D))
        {
            player.Move(new Vector3(player.transform.right.x * tSpeed, vSpeed, player.transform.right.z * tSpeed));
        }
    }
    public void changeState(playerState state)
    {
        this.state = state;
    }
    public playerState GetPlayerState()
    {
        return state;
    }
}
