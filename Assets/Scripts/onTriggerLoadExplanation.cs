﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class onTriggerLoadExplanation : MonoBehaviour {

    public GameObject actionText;
    public GameObject textPanel;
    public GameObject particles;
	
	void Start () {
        actionText.SetActive(false);
        textPanel.SetActive(false);
        particles.GetComponent<ParticleSystem>().Stop();
	}
	void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            actionText.SetActive(true);
            actionText.GetComponentInChildren<Text>().text = "Press [Space bar] to talk";
            if (actionText.activeInHierarchy == true && Input.GetKeyDown(KeyCode.Space))
            {
                textPanel.SetActive(true);
                other.gameObject.GetComponent<PlayerCharacterController>().changeState(playerState.talking);
            }
        }
    }
    void OnTriggerExit()
    {
        actionText.SetActive(false);
        textPanel.SetActive(false);
    }
}
