﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Read_FillFromArchive : MonoBehaviour   {
    
    //private string dvString = "";
    
    public int[,] fillMatrix(TextAsset file)
    {
        try
        {
            int[,] matrix;
            int m, i = 0, temp = 0, cont = 0;
            string mytext = file.text;
            string[] values;
            string[] lines;

            lines = file.text.Split(new[] { "\n", "\r\n", "\r" }, System.StringSplitOptions.None);
            m = System.Convert.ToInt32(lines[0]);
            //print("m: " + m);
            matrix = new int[m, m];
            for (i = 0; i < m; i++)
            {
                values = lines[i + 1].Split(new[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
                for (int j = i; j < m; j++)
                {
                    if (i == j)
                    {
                        matrix[i, j] = int.MaxValue;
                        continue;
                    }
                    temp = System.Convert.ToInt32(values[cont++]);
                    if (temp == -1)
                        temp = int.MaxValue;
                    matrix[i, j] = temp;
                    matrix[j, i] = temp;
                }
                cont = 0;
            }
            //for (i = 0; i < m; i++)
            //{
            //    for (int j = 0; j < m; j++)
            //    {
            //        dvString += matrix[i, j] + " ";
            //    }
            //    dvString += "\n";
            //}
            //print("llenado completo: \n"+dvString);
            return matrix;
        }
        catch (System.Exception e)
        {
            Debug.Log("uff Error! en:" + e.Message);
        }
        return new int[,] { };
    }
    public string [] getDialogueArray(TextAsset file)
    {
        string[] lines;
        string[] tutorial;
        lines = file.text.Split(new[] { "\n"}, System.StringSplitOptions.None);
        int n = System.Convert.ToInt32(lines[0]);
        tutorial = new string[n];
        print(tutorial.Length);
        for (int i =0; i<n; i++)
        {
            tutorial[i] = lines[i + 1];
        }
        return tutorial;
    }
}
