﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoviePlayer : MonoBehaviour
{

    public MovieTexture movieTexture;
    
    public void startMovie()
    {
        if (movieTexture != null)
        {
            movieTexture.Play();
        }
    }
    void OnGUI()
    {
        if (movieTexture != null && movieTexture.isPlaying)
        {
            GUI.DrawTexture(new Rect(0, 0,Screen.width,Screen.height),movieTexture,ScaleMode.StretchToFill);
        }
    }
}