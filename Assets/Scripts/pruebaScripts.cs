﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pruebaScripts : MonoBehaviour {
    public Texture2D texture;
    [Range(0.1f, 1f)]
    public float fadeSpeed = 0.2f;
    public int drawDepth = -1000;

    private float alpha = 1f;
    public int fadeDir = -1;
    private bool toFade = false;
    float initAlpha;
    void Start()
    {
        initAlpha = alpha;
    }
    void OnGUI()
    {
        if (initAlpha == 1 && alpha>0 || initAlpha == 0 && alpha <=1)
        { 
            print("alpha: " + alpha);
            alpha += fadeDir * fadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);

            Color newColor = GUI.color;
            newColor.a = alpha;
            GUI.color = newColor;
            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
        }
    }
}

