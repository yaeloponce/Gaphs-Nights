﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HighlightNodes : MonoBehaviour {
    public enum estado
    {
        noVisitado = 0,
        actual = 1,
        visitado = 2
    }

    estado prueba;
    private Button myButton;
    private GameObject[] tableHiglighters;
    private GameObject[] nodeHighLighters;
    private int actual_dist = -1;
    private int actual_node=-1;
    private int old_node=-1;
    private DijkstraManager dijstraManager;

    private Text dialogueText;

    void Start()
    {
        this.prueba = estado.noVisitado;
        tableHiglighters = new GameObject[GameObject.FindGameObjectsWithTag("HL_table").Length];
        nodeHighLighters = new GameObject[GameObject.FindGameObjectsWithTag("HL_node").Length];
        dijstraManager = FindObjectOfType<DijkstraManager>();
        for (int i = 0; i < tableHiglighters.Length; i++){
            tableHiglighters[i] = GameObject.Find("hl"+ (i+1));
            nodeHighLighters[i] = GameObject.Find("hlNode" + (i+1));
        }
        actual_node = dijstraManager.getActualVertex();
        dialogueText = GameObject.Find("DialogueText").GetComponent<Text>();
    }
    public void updateActualVertex()
    {
        //old_node = actual_node;
        prueba = estado.visitado;
        changeHighlightState();
        actual_node = dijstraManager.getActualVertex();
        actual_dist = dijstraManager.getActualDist();
        prueba = estado.actual;
    }
    public void changeHighlightState()
    {
        //print("actual "+actual_node);
        if (actual_node < 0)
        {
            return;
        }
        if (dijstraManager.isFinished())
        {
            FinishScene();
        }
        //this.prueba = entrante;
        //print("if "+tableHiglighters[actual_node].name+" vs "+gameObject.name);
        if (!tableHiglighters[actual_node-1].Equals(gameObject))
        {
            //print("no es el nodo que sigue "+tableHiglighters[actual_node-1].name+", "+gameObject.name);
            dialogueText.text = "Fíjate bien en las opciones, esa no es la menor.";
            return;
        }
        //print(gameObject.name);
        ColorBlock cb = gameObject.GetComponent<Button>().colors;
        switch (prueba)
        {
            case estado.noVisitado:
                cb.normalColor = Color.white;
                cb.highlightedColor = Color.white;
                //cb.pressedColor = Color.white;
                gameObject.GetComponent<Button>().colors = cb;
                //nodeHighLighters[actual_node].GetComponent<Image>().color = Color.blue;
                dialogueText.text = "Epale.";
                break;
            case estado.actual:
                cb.normalColor = Color.red;
                cb.highlightedColor = Color.red;
                //cb.pressedColor = Color.red;
                gameObject.GetComponent<Button>().colors = cb;
                nodeHighLighters[actual_node-1].GetComponent<Image>().color = Color.yellow;
                //cambiar el texto del botón de suma
                GameObject.FindObjectOfType<SumButton>().updateText(actual_dist);
                GameObject.FindObjectOfType<PathShower>().hightlightPath(actual_node);
                dialogueText.text = "Excelente, ahora repite los pasos empezando desde el nodo actual.";
                break;
            case estado.visitado:
                cb.normalColor = Color.gray;
                cb.highlightedColor = Color.gray;
                //cb.pressedColor = Color.gray;
                gameObject.GetComponent<Button>().colors = cb;
                nodeHighLighters[actual_node-1].GetComponent<Image>().color = Color.gray;
                GameObject.FindObjectOfType<PathShower>().turnOffAllPaths();
                dialogueText.text = "Este nodo ya lo visitaste y por lo tanto ya no está disponible, selecciona uno disponible.";
                break;
        }
    }
    public void FinishScene()
    {
        var lvlManager = GameObject.FindObjectOfType<LevelManager>();
        StartCoroutine(lvlManager.LoadScene());
    }
}
