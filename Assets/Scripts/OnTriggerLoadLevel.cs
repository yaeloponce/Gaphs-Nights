﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class OnTriggerLoadLevel : MonoBehaviour {
    public GameObject guiObject;
    public string levelToLoad;
    [Tooltip("Add the texture to show while fading the scene")]
    public Texture2D TextureToFade;
	// Use this for initialization
	void Start () {
        guiObject.SetActive(false);
	}
	void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            guiObject.SetActive(true);
            guiObject.GetComponent<Text>().text = "Press [Space bar] to enter";
            if (guiObject.activeInHierarchy == true && Input.GetButton("Use"))
            {
                AudioSource gongSound = gameObject.AddComponent<AudioSource>();
                gongSound.PlayOneShot((AudioClip)Resources.Load("gong sound FX"));
                LevelManager lvlManager = GameObject.FindObjectOfType<LevelManager>();
                lvlManager.LoadScene(TextureToFade, levelToLoad);
            }
        }
    }
    void OnTriggerExit()
    {
        guiObject.SetActive(false);
    }
}
