﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    public string SceneToGo;
    public Texture2D texture;
    FadeInOutScene fader;
    void Start()
    {
        fader = gameObject.AddComponent<FadeInOutScene>();
        fader.texture = texture;
        fader.Fade(InOut.FadeIn);
    }
	public void QuitGame(){
		Application.Quit ();
	}
    public IEnumerator LoadScene()
    {
        StartCoroutine(fader.Fade(InOut.FadeOut));
        yield return new WaitUntil(() => fader.GetAlpha() == 1.0f);
        SceneManager.LoadScene(SceneToGo);
    }
    public IEnumerator LoadScene(string sceneToGo)
    {
        StartCoroutine(fader.Fade(InOut.FadeOut));
        yield return new WaitUntil(()=>fader.GetAlpha() >= 0.97f);
        SceneManager.LoadScene(sceneToGo);
    }
    public IEnumerator LoadScene(Texture2D texture, string sceneToGo)
    {
        StartCoroutine(fader.Fade(texture, InOut.FadeOut));
        yield return new WaitUntil(() => fader.GetAlpha() == 1.0f);
        SceneManager.LoadScene(sceneToGo);
    }
}
