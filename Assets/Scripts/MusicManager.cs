﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {
    //private static MusicManager instance;
    //public static MusicManager Instance
    //{
    //    get { return instance; }
    //}
    private GameObject musicPlayer;
	void Awake () {
        musicPlayer = GameObject.Find("MUSIC");
        if (musicPlayer != null)
        {
            print("destroyed: " + musicPlayer.name);
            Destroy(musicPlayer.gameObject);
        }
        musicPlayer = GameObject.Find("Music");
        if (musicPlayer != null)
        {
            print("changing name of "+musicPlayer.name);
            musicPlayer.name = "MUSIC";
            DontDestroyOnLoad(musicPlayer);
        }
	}
	
}
