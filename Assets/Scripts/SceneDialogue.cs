﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum action
{
    SceneChange, activateTrigger
}
[RequireComponent(typeof(Text))]
public class SceneDialogue : MonoBehaviour {

    [Tooltip("Select what action do at the end of the dialogue")]
    public action actionToDo = action.SceneChange;

    [Tooltip("Name of the Scene to change or the tag of the triggers")]
    public string actionName;
    private Text textComponent;

    [Tooltip("The size it's the ammount of paragraphs to show, each element it's a paragraph")]
    public TextAsset dialogueFile;
    string[] DialogueStrings;

    //public float charDelay= 0.0001f;
    //public float CharacterRateMultiplier = 0.5f;

    public KeyCode DialogueInput = KeyCode.Space;

    private bool isStringBeingRevelated = false;
    private bool isDialoguePlaying = false;
    private bool isEndOfDialogue = false;

    public GameObject continueIcon;
    public GameObject stopIcon;

	// Use this for initialization
	void Start () {
		textComponent = GetComponent<Text> ();
		textComponent.text = "Presone [Espacio] para continuar";
        Read_FillFromArchive rf = gameObject.AddComponent<Read_FillFromArchive>();
        DialogueStrings = rf.getDialogueArray(dialogueFile);
        HideIcons();
        ShowIcon();
        //isDialoguePlaying = true;
        //StartCoroutine(StartDialogue());
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (DialogueInput)) {
            if (!isDialoguePlaying)
            {
                isDialoguePlaying = true;
                StartCoroutine (StartDialogue());
            }
			
		}
	}
    private IEnumerator StartDialogue()
    {
        int dialogueLength = DialogueStrings.Length;
        //int dialogueLength = 2;
        int currentDialogueIndex = 0;
        while (currentDialogueIndex < dialogueLength /*|| !isStringBeingRevelated*/)
        {
            if (!isStringBeingRevelated)
            {
                isStringBeingRevelated = true;
                StartCoroutine(DisplaySrings(DialogueStrings[currentDialogueIndex++]));
                //print("current Index: "+currentDialogueIndex);
                if (currentDialogueIndex >= dialogueLength)
                {
                    isEndOfDialogue = true;
                }
            }
            yield return 0;
        }
        while (true)
        {
            if (!isStringBeingRevelated && Input.GetKey(DialogueInput))
            {
                break;
            }
            yield return 0;
        }

        if (isEndOfDialogue && actionToDo == action.SceneChange)
        {
            try
            {
                var lvlManager = GameObject.FindObjectOfType<LevelManager>();
                StartCoroutine(lvlManager.LoadScene(actionName));
                //SceneManager.LoadScene(actionName);
            }
            catch (System.Exception e)
            {
                print("No pudo cargarse la escena: \""+actionName+"\" \n"+e.StackTrace);
            }
            
        }
        else if(isEndOfDialogue && actionToDo == action.activateTrigger)
        {
            GameObject door = GameObject.Find(actionName);
            door.GetComponent<BoxCollider>().enabled = true;
            door.GetComponentInChildren<ParticleSystem>().Play();
            PlayerCharacterController player = GameObject.FindObjectOfType<PlayerCharacterController>();
            player.changeState(playerState.normal);
        }

        HideIcons();
        
        isEndOfDialogue = false;
        isDialoguePlaying = false;
    }

	private IEnumerator DisplaySrings(string stringToDisplay){
		int stringLenght = stringToDisplay.Length;
		int currentCharacterIndex = 0;

        HideIcons();

		textComponent.text = "";

		while (currentCharacterIndex < stringLenght) {

			textComponent.text += stringToDisplay [currentCharacterIndex];
			currentCharacterIndex++;

			if (currentCharacterIndex < stringLenght)
            {
                //if (Input.GetKey(DialogueInput))
                //{
                //    //yield return new WaitForSeconds(charDelay*0.1f);
                //    yield return new WaitForSeconds(charDelay * CharacterRateMultiplier);
                //    //textComponent.text += "********";
                //    //currentCharacterIndex=stringToDisplay.Length-1;

                //}
                //else
                //{
                //    yield return new WaitForSeconds(charDelay);
                //}
                yield return new WaitForSeconds(0.005f*Time.deltaTime);
            }
            else
            {
				break;
			}
		}
        
        ShowIcon();

        while (true)
        {
            if (Input.GetKey(DialogueInput))
            {
                break;
            }
            yield return 0;
        }
        
        HideIcons();

        isStringBeingRevelated = false;
        if(actionToDo == action.SceneChange)
        {
            textComponent.text = "Loading...";
        }
        else
        {
            textComponent.text = "Si gustas que lo repita, sigue hablando conigo.\nSi comprendiste todo, puedes proceder por esa puerda de atrás y te sigo contando en el camino.";
        }
		
	}

    private void HideIcons()
    {
        continueIcon.SetActive(false);
        stopIcon.SetActive(false);
    }

    private void ShowIcon()
    {
        if (isEndOfDialogue)
        {
            stopIcon.SetActive(true);
            return;
        }
        continueIcon.SetActive(true);
    }
}
