﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    private Rigidbody player;
    public float speed;
    void Start()
    { 
            player = GetComponent<Rigidbody>();
    }
	void FixedUpdate()
    {
            if (Input.GetKey(KeyCode.W))
            {
                player.AddForce(player.transform.forward * speed);
                //transform.Translate(Vector3.forward);
                //transform.parent.gameObject.transform.Translate( Vector3.forward * zoomSpeed);
            }
            if (Input.GetKey(KeyCode.S))
            {
                player.AddForce(-player.transform.forward * speed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                player.AddForce(-player.transform.right * speed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                player.AddForce(player.transform.right * speed);
            }
    }
}
