﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimedDialogue : MonoBehaviour {
    public TextAsset file;
    public float DialogueTimeStart;
    public float DialogueDuration;
    Text TextComponent;
    string[] tutorial;
    private int i = 0;

	// Use this for initialization
	void Start () {
        Read_FillFromArchive archive = gameObject.AddComponent<Read_FillFromArchive>();
        tutorial = archive.getDialogueArray(file);
        TextComponent = gameObject.GetComponent<Text>();
        
	}
    public void startRepeating()
    {
        InvokeRepeating("updateTutorial", DialogueTimeStart, DialogueDuration);
    }
	void updateTutorial()
    {
        TextComponent.text = tutorial[i++];
        if (i == tutorial.Length)
            i = 0;
    }
}
