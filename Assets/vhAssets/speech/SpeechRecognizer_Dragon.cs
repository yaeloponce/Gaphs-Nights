﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class SpeechRecognizer_Dragon : SpeechRecognizer
{
    #region Constants

    #endregion

    #region Variables
    public string m_User = "adam";
    public string m_Topic = "General - Medium";
    public int m_TimeOut = 30;
    #endregion

    #region Functions
    void Start()
    {

    }

    protected override void PerformRecognition(AudioClip clip)
    {
        string wavPath = VHUtils.GetStreamingAssetsPath() + "Dragon/testwav.wav";
        AudioConverter.ConvertClipToWav(clip, wavPath);
        StartCoroutine(TranscribeAudio(m_User, m_Topic, wavPath, Path.ChangeExtension(wavPath, ".txt"), m_TimeOut));
    }

    IEnumerator TranscribeAudio(string user, string topic, string wavPath, string outputTextPath, int timeout)
    {
#if !UNITY_WEBPLAYER
        System.Diagnostics.Process transcriptionProcess = new System.Diagnostics.Process();
        transcriptionProcess.StartInfo.FileName = VHUtils.GetStreamingAssetsPath() + "Dragon/trscribe_ftof.exe";
        transcriptionProcess.StartInfo.Arguments = string.Format(@"-username=""{0}"" -topicname=""{1}"" -wavefile=""{2}"" -outfile=""{3}"" -timeout={4}",
            string.Format("{0}Dragon/Users/{1}", VHUtils.GetStreamingAssetsPath(), user), topic, wavPath, outputTextPath, timeout);
        transcriptionProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        transcriptionProcess.Start();
        //transcriptionProcess.WaitForExit();

        while (!transcriptionProcess.HasExited)
        {
            yield return new WaitForEndOfFrame();
        }
#endif

        // read the transcription
        string transcription = "";
#if !UNITY_WEBPLAYER
        if (File.Exists(outputTextPath))
        {
            transcription = File.ReadAllText(outputTextPath);
        }
#endif

        List<RecognizerResult> recognizerResults = new List<RecognizerResult>();
        recognizerResults.Add(new RecognizerResult(transcription));
        DispatchResults(recognizerResults);

#if UNITY_WEBPLAYER
        yield break;
#endif
    }
    #endregion
}
