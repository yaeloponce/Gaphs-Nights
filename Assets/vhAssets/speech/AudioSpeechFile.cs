﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AudioSpeechFile : MonoBehaviour
{
    #region Variables
    public TextAsset m_LipSyncInfo;
    public TextAsset m_UtteranceText;
    public TextAsset m_Xml;
    public AudioClip m_AudioClip;
    List<BMLParser.BMLTiming> m_WordTimings = new List<BMLParser.BMLTiming>();
    List<BMLParser.LipData> m_LipData = new List<BMLParser.LipData>();
    List<BMLParser.CurveData> m_CurveData = new List<BMLParser.CurveData>();
    BMLParser m_Parser;
    string m_ConvertedXml = "";
    #endregion

    #region Properties
    public float Length
    {
        get { return m_WordTimings.Count > 0 ? m_WordTimings[m_WordTimings.Count - 1].time : 0; }
    }

    public float ClipLength
    {
        get { return m_AudioClip.length; }
    }

    public string UtteranceText
    {
        get { return m_UtteranceText.text; }
    }
    #endregion

    #region Functions
    void Start()
    {
        ReadBmlData();
        if (m_Xml != null)
        {
            m_ConvertedXml = ConvertXmlToSmartbodyReadable(m_Xml.text);
        }
    }

    void OnParsedWordTiming(BMLParser.BMLTiming wordTiming)
    {
        m_WordTimings.Add(wordTiming);
    }

    void OnParsedLipData(BMLParser.LipData lipData)
    {
        m_LipData.Add(lipData);
    }

    void OnParsedCurveData(BMLParser.CurveData curveData)
    {
        m_CurveData.Add(curveData);
    }

    public string GetUtteranceText()
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < m_WordTimings.Count; i++)
        {
            builder.Append(m_WordTimings[i].text + " ");
        }
        return builder.ToString();
    }

    public void ReadBmlData()
    {
        if (m_Parser == null)
        {
            m_Parser = new BMLParser(OnParsedWordTiming, OnParsedLipData, OnParsedCurveData);
        }

        if (m_LipSyncInfo != null)
        {
            m_Parser.LoadBMLString(m_LipSyncInfo.text, false);
        }
        else
        {
            Debug.LogError("There is no lip sync file assigned to utterance " + name);
        }
    }

    public void PlayLipSync(string character)
    {
        if (m_LipSyncInfo != null)
        {
            SmartbodyManager.Get().SBPlayAudio(character, System.IO.Path.GetFileNameWithoutExtension(m_LipSyncInfo.name));
        }
        else
        {
            Debug.LogError("There is no lip sync file assigned to utterance " + name);
        }
    }

    public void PlayXml(string character)
    {
        if (m_Xml != null)
        {
            string message = string.Format(@"bml.execXML('{0}', '{1}')", character, m_ConvertedXml);
            SmartbodyManager.Get().PythonCommand(message);
        }
        else
        {
            Debug.LogError("There is no xml file assigned to utterance " + name);
        }
    }

    string ConvertXmlToSmartbodyReadable(string xmlContents)
    {
        string bml = xmlContents;
        bml = bml.Replace(@"<?xml version=""1.0""?>", "");
        bml = bml.Replace(@"<?xml version=""1.0"" encoding=""utf-8""?>", "");
        bml = bml.Replace(@"\r\n", "");
        bml = bml.Replace(@"\n", "");
        bml = bml.Replace(System.Environment.NewLine, "");
        //Debug.Log(bml);
        return bml;
    }
    #endregion
}
