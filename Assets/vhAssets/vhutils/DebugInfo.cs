﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugInfo : MonoBehaviour
{
    #region Constants

    public enum InfoMode
    {
        Off,
        General,
        System,

        NUM_MODES
    }

    #endregion


    #region Variables

    public FpsCounter m_fpsCounter;
    public Camera m_Camera;
    public KeyCode m_ToggleKey = KeyCode.None;
    public Vector2 m_ScreenPosition = Vector2.zero;  // in pixels
    public Color m_color = Color.white;
    public float m_lowFpsThreshold = 30;
    public List<string> m_additionalLinesPersistent = new List<string>();
    public List<string> m_additionalLinesPerFrame = new List<string>();
    InfoMode m_Mode = InfoMode.Off;

    #endregion


    #region Functions

    void Start()
    {
        if (m_fpsCounter == null)
        {
            m_fpsCounter = GameObject.FindObjectOfType<FpsCounter>();
        }
    }


    void Update()
    {
        if (Input.GetKeyDown(m_ToggleKey))
        {
            NextMode();
        }
    }


    void OnGUI()
    {
        switch (m_Mode)
        {
            case InfoMode.General:
                DrawGeneralInfo();
                break;

            case InfoMode.System:
                DrawSystemInfo();
                break;
        }

        if (Event.current.type == EventType.repaint)
        {
            m_additionalLinesPerFrame.Clear();
        }
    }


    public void NextMode()
    {
        ++m_Mode;
        m_Mode = (InfoMode)((int)m_Mode % (int)InfoMode.NUM_MODES);
    }


    public void SetMode(InfoMode mode)
    {
        m_Mode = mode;
    }


    void DrawGeneralInfo()
    {
        float fps = 0;
        float averageFps = 0;
        if (m_fpsCounter)
        {
            fps = m_fpsCounter.Fps;
            averageFps = m_fpsCounter.AverageFps;
        }

        Camera camera = m_Camera;
        if (camera == null)
            camera = Camera.main;


        GUILayout.BeginArea(new Rect(m_ScreenPosition.x, m_ScreenPosition.y, Screen.width, Screen.height));
        GUILayout.BeginVertical();

        GUI.color = m_color;

        float fpsColor = Mathf.Min(1.0f, averageFps / m_lowFpsThreshold);
        VHGUILayout.Label(string.Format("T: {0:f2} F: {1} AVG: {2:f0} FPS: {3:f2}", Time.time, Time.frameCount, averageFps, fps), new Color(1, fpsColor, fpsColor));
        GUILayout.Label(string.Format("{0}x{1}x{2} ({3})", Screen.width, Screen.height, Screen.currentResolution.refreshRate, VHUtils.GetCommonAspectText((float)Screen.width / Screen.height)));
        GUILayout.Label(string.Format("{0}", Application.loadedLevelName));
        GUILayout.Label(string.Format("{0}", QualitySettings.names[QualitySettings.GetQualityLevel()]));

        if (camera != null)
        {
            Transform camTrans = camera.transform;
            GUILayout.Label(string.Format("Cam Pos ({0}): {1:f2} {2:f2} {3:f2}", camera.name, camTrans.position.x, camTrans.position.y, camTrans.position.z));
            GUILayout.Label(string.Format("Cam Rot (xyz): {0:f2} {1:f2} {2:f2}", camTrans.rotation.eulerAngles.x, camTrans.rotation.eulerAngles.y, camTrans.rotation.eulerAngles.z));
        }

        foreach (string text in m_additionalLinesPersistent)
        {
            GUILayout.Label(text);
        }

        foreach (string text in m_additionalLinesPerFrame)
        {
            GUILayout.Label(text);
        }

        GUI.color = Color.white;

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }


    void DrawSystemInfo()
    {
        GUILayout.BeginArea(new Rect(m_ScreenPosition.x, m_ScreenPosition.y, Screen.width, Screen.height));
        GUILayout.BeginVertical();

        GUI.color = m_color;

        GUILayout.Label(string.Format("{0}", SystemInfo.operatingSystem));  // Operating system name with version (Read Only).
        GUILayout.Label(string.Format("{0} x {1}", SystemInfo.processorCount, SystemInfo.processorType));  // Processor name (Read Only).
        GUILayout.Label(string.Format("Mem: {0:f1}gb", SystemInfo.systemMemorySize / 1000.0f));  // Amount of system memory present (Read Only).
        GUILayout.Label(string.Format("{0} - deviceID: {1}", SystemInfo.graphicsDeviceName, SystemInfo.graphicsDeviceID));  // The name of the graphics device (Read Only).  - The identifier code of the graphics device (Read Only).
        GUILayout.Label(string.Format("{0} - vendorID: {1}", SystemInfo.graphicsDeviceVendor, SystemInfo.graphicsDeviceVendorID));  // The vendor of the graphics device (Read Only).  - The identifier code of the graphics device vendor (Read Only).
        GUILayout.Label(string.Format("{0}", SystemInfo.graphicsDeviceVersion));  // The graphics API version supported by the graphics device (Read Only).
        GUILayout.Label(string.Format("VMem: {0}mb", SystemInfo.graphicsMemorySize));  // Amount of video memory present (Read Only).
        GUILayout.Label(string.Format("Shader Level: {0:f1}", SystemInfo.graphicsShaderLevel / 10.0f));  // Graphics device shader capability level (Read Only).
#if UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6
        GUILayout.Label(string.Format("Fillrate: {0}", SystemInfo.graphicsPixelFillrate.ToString()));  // Approximate pixel fill-rate of the graphics device (Read Only).
#endif
#if UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6
        GUILayout.Label(string.Format("Shadows:{0} RT:{1} FX:{2}", SystemInfo.supportsShadows ? "y" : "n", SystemInfo.supportsRenderTextures ? "y" : "n", SystemInfo.supportsImageEffects ? "y" : "n"));  // Are built-in shadows supported? (Read Only)
#else
        GUILayout.Label(string.Format("Shadows:{0} RT:{1} FX:{2} MT:{3}", SystemInfo.supportsShadows ? "y" : "n", SystemInfo.supportsRenderTextures ? "y" : "n", SystemInfo.supportsImageEffects ? "y" : "n", SystemInfo.graphicsMultiThreaded ? "y" : "n"));  // Are built-in shadows supported? (Read Only)
#endif
        GUILayout.Label(string.Format("deviceUniqueIdentifier: {0}", SystemInfo.deviceUniqueIdentifier));  // A unique device identifier. It is guaranteed to be unique for every device (Read Only)
        GUILayout.Label(string.Format("deviceName: {0}", SystemInfo.deviceName));  // The user defined name of the device (Read Only).
        GUILayout.Label(string.Format("deviceModel: {0}", SystemInfo.deviceModel));  // The model of the device (Read Only).
        GUILayout.Label(string.Format("deviceType: {0}", SystemInfo.deviceType));  // Returns the kind of device the application is running on.
        //GUILayout.Label(string.Format("{0}", SystemInfo.npotSupport));   // What NPOTSupport support does GPU provide? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportedRenderTargetCount));   // How many simultaneous render targets (MRTs) are supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supports3DTextures));   // Are 3D (volume) textures supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsAccelerometer));   // Is an accelerometer available on the device?
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsComputeShaders));   // Are compute shaders supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsGyroscope));   // Is a gyroscope available on the device?
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsInstancing));   // Is GPU draw call instancing supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsLocationService));   // Is the device capable of reporting its location?
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsRenderToCubemap));   // Are cubemap render textures supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsSparseTextures));   // Are sparse textures supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsStencil));   // Is the stencil buffer supported? (Read Only)
        //GUILayout.Label(string.Format("{0}", SystemInfo.supportsVibration));   // Is the device capable of providing the user haptic feedback by vibration?

        GUI.color = Color.white;

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    #endregion
}
