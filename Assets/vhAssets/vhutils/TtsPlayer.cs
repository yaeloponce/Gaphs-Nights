﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;

public class TtsPlayer : MonoBehaviour
{
    #region Constants
    const string animName = "test";
    const int NumFrames = 20;
    #endregion

    #region Variables
    public TextAsset m_FaceFxFile;
    public FaceFXControllerScript m_FaceFXScript;
    Dictionary<string, List<FaceFXControllerScript.AnimClipHelper>> m_DefaultMouthShapes;
    #endregion

    #region Properties
    //AnimationState neutral { get { return animation["face_neutral"]; } }
    //AnimationState fv { get { return animation["FV"]; } }
    //AnimationState open { get { return animation["open"]; } }
    //AnimationState pbm { get { return animation["PBM"]; } }
    //AnimationState shch { get { return animation["ShCh"]; } }
    //AnimationState troof { get { return animation["tRoof"]; } }
    //AnimationState tback { get { return animation["tBack"]; } }
    //AnimationState tteeth { get { return animation["tTeeth"]; } }
    //AnimationState w { get { return animation["W"]; } }
    //AnimationState wide { get { return animation["wide"]; } }
    #endregion

    #region Functions
    // Searches the object this script is attached to recursively to find a match.  We can't use GameObject.Find because that searches the whole scene.  Transform.Find searches one level.
    Transform RecursiveFind(Transform trans, string searchName)
    {
        foreach (Transform child in trans)
        {
            if (child.name == searchName)
            {
                return child;
            }
            Transform returnTransform = RecursiveFind(child, searchName);
            if (returnTransform != null)
            {
                return returnTransform;
            }
        }
        return null;
    }

    void Start()
    {
        m_DefaultMouthShapes = m_FaceFXScript.ImportXML(m_FaceFxFile.text, false);

        //foreach (KeyValuePair<string, List<FaceFXControllerScript.AnimClipHelper>> kvp in m_DefaultMouthShapes)
        //{
        //    for (int i = 0; i < kvp.Value.Count; i++)
        //    {
        //        Debug.Log(kvp.Key + " " + kvp.Value[i].relativePathName);
        //    }
        //}

        //neutral.wrapMode = WrapMode.Loop;
        //neutral.layer = 0;
        //animation.Play(neutral.clip.name);
    }

    public AnimationClip BuildTTSAnimation(List<TtsReader.WordTiming> timings, bool playAnim)
    {
        AnimationClip finalAnim = new AnimationClip();
        finalAnim.name = "test";
        List<FaceFXControllerScript.AnimClipHelper> finalAnimCurves = new List<FaceFXControllerScript.AnimClipHelper>();
        //float lastKeyFrameTime = 0;

        for (int wordTimingIndex = 0; wordTimingIndex < timings.Count; wordTimingIndex++)
        {
            //TtsReader.WordTiming word = timings[wordTimingIndex];
            for (int mouthShapeIndex = 0; mouthShapeIndex < timings[wordTimingIndex].m_VisemesUsed.Count; mouthShapeIndex++)
            {
                TtsReader.VisemeData vData = timings[wordTimingIndex].m_VisemesUsed[mouthShapeIndex];
                if (m_DefaultMouthShapes.ContainsKey(vData.type))
                {
                    //lastKeyFrameTime = vData.start;
                    BuildAnimationCurve(vData.start, vData.articulation, m_DefaultMouthShapes[vData.type], finalAnimCurves, finalAnim);
                }
            }
        }

        List<TtsReader.VisemeData> lastVisemes = timings[timings.Count - 1].m_VisemesUsed;
        for (int i = 0; i < lastVisemes.Count; i++)
        {
            TtsReader.VisemeData vData = lastVisemes[i];
            if (m_DefaultMouthShapes.ContainsKey(vData.type))
            {
                BuildAnimationCurve(vData.start + 0.1f, 0, m_DefaultMouthShapes[vData.type], finalAnimCurves, finalAnim);
            }
        }

        //Debug.Log("finalAnimCurves: " + finalAnimCurves.Count);
        for (int i = 0; i < finalAnimCurves.Count; i++)
        {
            //Debug.Log("finalAnimCurves[i].relativePathName: " + finalAnimCurves[i].relativePathName);
            //finalAnimCurves[i].SetEvaulatedFrameData(lastKeyFrameTime + 0.1f, baseCurves[i].GetEvaluatedFrameData(0, 1.0f));
            finalAnimCurves[i].PostAddKeys(finalAnimCurves[i].relativePathName);
        }

        GetComponent<Animation>().AddClip(finalAnim, finalAnim.name);
        return finalAnim;
    }

    void BuildAnimationCurve(float keyTime, float weight, List<FaceFXControllerScript.AnimClipHelper> baseCurves, List<FaceFXControllerScript.AnimClipHelper> finalAnimCurves, AnimationClip finalAnim)
    {
        for (int i = 0; i < baseCurves.Count; i++)
        {
            int foundIndex = finalAnimCurves.FindIndex(c => baseCurves[i].relativePathName == c.relativePathName);
            FaceFXControllerScript.AnimClipHelper helper;
            if (foundIndex == -1)
            {
                helper = new FaceFXControllerScript.AnimClipHelper(finalAnim);
                helper.relativePathName = baseCurves[i].relativePathName;
                finalAnimCurves.Add(helper);
            }
            else
            {
                helper = finalAnimCurves[foundIndex];
            }

            helper.SetEvaulatedFrameData(keyTime, baseCurves[i].GetEvaluatedFrameData(weight));
        }
    }

    //string GetTransformPath(Transform t, bool includeTopNode)
    //{
    //    string retVal = "";
    //    Transform currNode = t;
    //    while (currNode.parent != null)
    //    {
    //        retVal = retVal.Insert(0, currNode.name + "/");
    //        currNode = currNode.parent;
    //    }

    //    if (includeTopNode)
    //    {
    //        retVal = retVal.Insert(0, currNode.name + "/");
    //    }

    //    retVal = retVal.Remove(retVal.Length - 1, 1);

    //    return retVal;
    //}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AnimationClip c = GetComponent<Animation>().GetClip("test");
            Debug.Log(c.length);
            m_FaceFXScript.PlayAnim(c.name, GetComponent<AudioSource>().clip);
        }
    }


    #endregion
}
