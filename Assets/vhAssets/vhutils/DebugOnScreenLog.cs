using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DebugOnScreenLog : MonoBehaviour
{
    class LogText
    {
        public string text;
        public Color color;
        public string stackTrace;
        public LogType logType;
        public float startTime;
        public bool display = false;  // to prevent one-frame issues with OnGUI() being called before Update()

        public LogText(string text, Color color, string stackTrace, LogType logType, float startTime) { this.text = text; this.color = color; this.stackTrace = stackTrace; this.logType = logType; this.startTime = startTime; }
    }


    public Rect m_displayPosition = new Rect(0.0f, 0.4f, 1.0f, 0.6f);

    public float m_normalDisplayTime  = 1.0f;
    public float m_warningDisplayTime = 3.0f;
    public float m_errorDisplayTime   = 8.0f;

    List<LogText> m_log = new List<LogText>();


    void Awake()
    {
        LogCallbackHandler handler = GameObject.FindObjectOfType<LogCallbackHandler>();
        if (handler)
            handler.AddCallback(LogCallback);
        else
            Debug.LogWarning("DebugOnScreenLog: LogCallbackHandler component not found in the scene.  Add one if you wish to display Unity Log() messages");
    }


    void Start()
    {
    }


    void Update()
    {
        for (int i = m_log.Count - 1; i >= 0; i--)
        {
            LogText log = m_log[i];

            log.display = true;

            float maxDisplayTime;
            switch (log.logType)
            {
                case LogType.Error:   maxDisplayTime = m_errorDisplayTime; break;
                case LogType.Warning: maxDisplayTime = m_warningDisplayTime; break;
                default:              maxDisplayTime = m_normalDisplayTime; break;
            }

            float currentDisplayTime = Time.time - log.startTime;
            float currentDisplayTimePercentage = currentDisplayTime / maxDisplayTime;
            const float alphaPercentage = 0.8f; // percentage of display time to start fading out

            if (currentDisplayTimePercentage > alphaPercentage)
            {
                log.color.a = (1 - currentDisplayTimePercentage) / (1 - alphaPercentage);
            }

            if (currentDisplayTime >= maxDisplayTime)
            {
                m_log.Remove(log);
            }
        }
    }


    void OnGUI()
    {
        {
            Rect rect = new Rect(m_displayPosition);

            rect.y += rect.height;
            VHGUI.ScaleToRes(ref rect);

            for (int i = m_log.Count - 1; i >= 0; i--)
            {
                LogText log = m_log[i];

                if (!log.display)
                    continue;

                rect.y -= GUI.skin.label.CalcHeight(new GUIContent(log.text), rect.width);

                GUI.color = log.color;
                GUI.Label(rect, log.text);
                GUI.color = Color.white;
            }
        }


        // gui butttons for testing Debug.Log().  Set to 'true' to enable
#if false
        {
            Rect r = new Rect(0.0f, 0.0f, 0.5f, 0.6f);
            GUILayout.BeginArea(VHGUI.ScaleToRes(ref r));
            GUILayout.BeginVertical();

            if (GUILayout.Button("Log"))
            {
                Debug.Log("Testing Log Error Message");
            }

            if (GUILayout.Button("Log Multiple"))
            {
                for (int i = 0; i < 100; i++)
                {
                    Debug.Log("Testing a bunch of Log Error Messages");
                }
            }

            if (GUILayout.Button("Log Long"))
            {
                string longError;
                longError = "124jh23k123l4h51kl3j5h1kl51 1jkl51lk4j5 h1lkj45h 1lk435h 1lk35h1lkj5h1lkj5h1lk5j 1klj45 1kl 51lkj54h 1l4k51klj45h1lkj54h1 l4k5 1kl4j5 1lk4j5 1lk45j 1l4kj51l k5 1lk4j5 1l45j1lkj51kl54jh1lkj51lk50998f890fa-0sd9fs0f90sdf 0s f0s df0sdf s90f8 0s9d8f s09df s09f s09 dfs90d fs09f8 09wer2 309523 l2k4j23l4 23l4j 2l34 2l3j4 2l4 2l34j 2l34 2l34l 2l4 2l";
                Debug.Log(longError);
            }

            if (GUILayout.Button("Warning"))
            {
                Debug.LogWarning("Testing Log Warning Message");
            }

            if (GUILayout.Button("Error"))
            {
                Debug.LogError("Testing Log Error Message");
            }

            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
#endif
    }


    void LogCallback(string logString, string stackTrace, LogType type)
    {
        // sanity check
        if (m_log.Count > 999)
            return;

        string logStringStripped = logString.Replace("<color=red>", "").Replace("</color>", "");

        switch (type)
        {
            case LogType.Error:
                m_log.Add(new LogText(logStringStripped, Color.red, stackTrace, type, Time.time));
                break;

            case LogType.Warning:
                m_log.Add(new LogText(logStringStripped, Color.yellow, stackTrace, type, Time.time));
                break;

            default:
                m_log.Add(new LogText(logStringStripped, Color.white, stackTrace, type, Time.time));
                break;
        }
    }
}
