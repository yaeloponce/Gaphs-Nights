using UnityEngine;
using System.Collections;

abstract public class ICharacter : MonoBehaviour
{
    #region Variables

    #endregion

    #region Properties
    public abstract string CharacterName { get; }
    #endregion

    #region Functions
    void Start()
    {

    }

    void Update()
    {

    }
    #endregion
}
