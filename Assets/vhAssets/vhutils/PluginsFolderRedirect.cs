using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class PluginsFolderRedirect
{
    // EDF - This component will redirect the Plugins folder to load C dlls from the Assets\Plugins folder
    //       On Windows, default behavior is to load C dlls from the Unity Editor installed folder, which
    //       is not what you want for your app.
    //       This calls the windows SetDllDirectory() to add Assets\Plugins to the dll search path.
    //       Note that C# dlls will load from Assets\Plugins without needing this redirect code.
    //       References:
    //       http://msdn.microsoft.com/en-us/library/windows/desktop/ms686203(v=vs.85).aspx
    //       https://confluence.ict.usc.edu/display/com/1079+-+External+.dlls
    //       https://jira.ict.usc.edu/browse/VH-164
    //       https://jira.ict.usc.edu/browse/VH-418

    static bool m_pluginsFolderRedirected = false;


    public static bool PluginsFolderRedirected { get { return m_pluginsFolderRedirected; } }


    public static bool RedirectPluginsFolder()
    {
        if (VHUtils.IsUnity5OrGreater())
            return true;

        if (!VHUtils.IsWindows())
            return true;

        if (!VHUtils.IsUnityPro())
            return true;

        if (PluginsFolderRedirected)
            return true;

        // in unity editor = /Assets/Plugins
        // in unity player = /App_Data/Plugins
        // dataPath points to both correctly
        string path = Application.dataPath + "/Plugins";
        bool successfullySetDllDirectory = SetDllDirectory(path);
        if (!successfullySetDllDirectory)
        {
            Debug.LogError(@"SetDllDirectory(""Assets/Plugins"") failed.  None of your dlls will work");
            return false;
        }

        m_pluginsFolderRedirected = true;

        return true;
    }


    [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool SetDllDirectory(string lpPathName);
}
