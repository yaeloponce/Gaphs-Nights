﻿using UnityEngine;
using System.Collections;
using System;

public class VideoRecorder : MonoBehaviour
{
    #region Variables
    public int m_RecordingMouseButton;
    public KeyCode m_RecordingKey;
    public bool m_CheckRecordingInput = true;
    public Renderer m_RenderTarget;
    WebCamTexture m_CurrentCam;
    #endregion

    #region Properties
    public WebCamTexture CurrentCam
    {
        get { return m_CurrentCam; }
    }

    public string CurrentCameraName
    {
        get { return CurrentCam != null ? CurrentCam.deviceName : ""; }
    }

    public WebCamDevice[] ConnectedDevices
    {
        get { return WebCamTexture.devices; }
    }

    public bool IsPlaying
    {
        get { return m_CurrentCam != null ? m_CurrentCam.isPlaying : false; }
    }

    public int NumConnectedDevices
    {
        get { return ConnectedDevices.Length; }
    }
    #endregion

    #region Functions
    void Start()
    {
        if (VHUtils.IsWebPlayer())
            StartCoroutine(WaitForConfirmation());
        else
            SetDefaultCamera();
    }

    IEnumerator WaitForConfirmation()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        SetDefaultCamera();
    }

    void SetDefaultCamera()
    {
        if (NumConnectedDevices > 0)
        {
            SetCamera(ConnectedDevices[0].name, m_RenderTarget);
        }

        if (NumConnectedDevices > 0)
        {
            foreach (WebCamDevice camera in ConnectedDevices)
                Debug.Log("VideoRecorder: " + camera.name);
        }
        else
        {
            Debug.Log("VideoRecorder: No camera devices detected");
        }
    }

    void Update()
    {
        if (m_CheckRecordingInput)
        {
            if (Input.GetMouseButton(m_RecordingMouseButton) || Input.GetKey(m_RecordingKey))
            {
                StartRecording();
            }
            else if (Input.GetMouseButtonUp(m_RecordingMouseButton) || Input.GetKeyUp(m_RecordingKey))
            {
                PauseRecording();
            }
        }
    }

    public void SetCamera(string deviceName, Renderer renderTarget)
    {
        if (m_CurrentCam != null)
        {
            m_CurrentCam.Stop();
        }
        m_CurrentCam = new WebCamTexture(deviceName);

        SetRenderTarget(renderTarget);
    }

    public void SetRenderTarget(Renderer renderTarget)
    {
        m_RenderTarget = renderTarget;
        if (m_RenderTarget != null)
        {
            m_RenderTarget.material.mainTexture = m_CurrentCam;
        }
        else
        {
            Debug.LogWarning("No render target set so the video won't be shown.  Call SetRenderTarget in order to set one");
        }
    }

    public void StartRecording()
    {
        if (m_CurrentCam != null)
        {
            m_CurrentCam.Play();
        }
        else
        {
            Debug.LogError("No camera set. Call SetCamera to set one");
        }
    }

    public void PauseRecording()
    {
        if (m_CurrentCam != null)
        {
            m_CurrentCam.Pause();
        }
        else
        {
            Debug.LogError("No camera set. Call SetCamera to set one");
        }
    }

    public void StopRecording()
    {
        if (m_CurrentCam != null)
        {
            m_CurrentCam.Stop();
        }
        else
        {
            Debug.LogError("No camera set. Call SetCamera to set one");
        }
    }

    public void PrintConnectedDevices()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log("Device Name: " + devices[i].name);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void SwitchCameras()
    {
        if (m_CurrentCam != null)
        {
            for (int i = 0; i < ConnectedDevices.Length; i++)
            {
                if (ConnectedDevices[i].name == CurrentCameraName)
                {
                    int newIndex = (i + 1) % NumConnectedDevices;
                    SetCamera(ConnectedDevices[newIndex].name, m_RenderTarget);
                }
            }
        }
        else
        {
            SetDefaultCamera();
        }
    }
    #endregion
}
