
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;


[CustomEditor(typeof(FaceFXControllerScript))]


public class FaceFXControllerScriptGUI : Editor
{

    // A class storing a bone transform.  The constructor takes the contents of a <bone> xml body (Space-seperated position rotation scale values).
    /*public struct BoneTransform 
    {
        public float[] Values;
        // Constructs a BoneTransform from a space-separated string (originating from an XML file)
        public BoneTransform ( string aValue  ){

          string[] StringValues = aValue.Split();
          Values = new float[10];
            if (StringValues.Length != 10)
            {
                Debug.Log("Error in XML.  A reference boen has only this many values:" + Values.Length ); 
                Debug.Log(aValue);
            }
            else
            {
                // Position (x, y, z)
                // @todo - Figure out why Pos.x and Rot.x values need to be negated.
                Values[0] = -float.Parse(StringValues[0]);  
                Values[1] = float.Parse(StringValues[1]);
                Values[2] = float.Parse(StringValues[2]);
			
                // Rotation (x, y, z, w but in the XML file it is stored as w,x,y,z)  
                Values[3] = -float.Parse(StringValues[4]);
                Values[4] = float.Parse(StringValues[5]);
                Values[5] = float.Parse(StringValues[6]);
                Values[6] = float.Parse(StringValues[3]);
			
                // Scale (x, y, z)
                Values[7] = float.Parse(StringValues[7]);
                Values[8] = float.Parse(StringValues[8]);
                Values[9] = float.Parse(StringValues[9]);
            }
       }
        public BoneTransform ( GameObject t  ){
            Values = new float[10];
           Values[0] = t.transform.localPosition.x;
           Values[1] = t.transform.localPosition.y;
           Values[2] = t.transform.localPosition.z;
           Values[3] = t.transform.localRotation.x;
           Values[4] = t.transform.localRotation.y;
           Values[5] = t.transform.localRotation.z;
           Values[6] = t.transform.localRotation.w;
           Values[7] = t.transform.localScale.x;
           Values[8] = t.transform.localScale.y;
           Values[9] = t.transform.localScale.z;  
       }
        public BoneTransform ( Vector3 pos ,   Quaternion rot ,   Vector3 scale  ){
            Values = new float[10];
           Values[0] = pos.x;
           Values[1] = pos.y;
           Values[2] = pos.z;
           Values[3] = rot.x;
           Values[4] = rot.y;
           Values[5] = rot.z;
           Values[6] = rot.w;
           Values[7] = scale.x;
           Values[8] = scale.y;
           Values[9] = scale.z;  
       }      
        public void Print (){
           Debug.Log("( " + Values[0] + ", " + Values[1] + ", " + Values[2]  + ") ("  + Values[3] +", " + Values[4] +", " + Values[5] +", " + Values[6] + ") ("+ Values[7] +", " + Values[8] +", " +Values[9] + ")");
        }
        public Vector3 GetPos (){
            return new Vector3(Values[0], Values[1], Values[2]);
        }
        public Quaternion GetRot (){
            return new Quaternion( Values[3], Values[4], Values[5], Values[6]);
        }
        public Vector3 GetScale (){
            return new Vector3(Values[7], Values[8], Values[9]);
        }
    }
    // A class to help manage adding keys to curves and curves to clips.
    struct AnimClipHelper 
    {

        AnimationCurve curvePosX; 
        AnimationCurve curvePosY; 
        AnimationCurve curvePosZ;
        AnimationCurve curveRotX;
        AnimationCurve curveRotY;
        AnimationCurve curveRotZ; 		
        AnimationCurve curveRotW;
        AnimationCurve curveScaleX; 
        AnimationCurve curveScaleY; 
        AnimationCurve curveScaleZ; 
        public AnimationClip animclip;

        public AnimClipHelper(AnimationClip clip){
            animclip = clip;
            curvePosX = new AnimationCurve();
            curvePosY = new AnimationCurve();
            curvePosZ = new AnimationCurve();
            curveRotX = new AnimationCurve();
            curveRotY = new AnimationCurve();
            curveRotZ = new AnimationCurve();
            curveRotW = new AnimationCurve();
            curveScaleX = new AnimationCurve();
            curveScaleY = new AnimationCurve();
            curveScaleZ = new AnimationCurve();		
        }

        public void PreAddKeys (){
            curvePosX = new AnimationCurve();
            curvePosY = new AnimationCurve();
            curvePosZ = new AnimationCurve();
            curveRotX = new AnimationCurve();
            curveRotY = new AnimationCurve();
            curveRotZ = new AnimationCurve();
            curveRotW = new AnimationCurve();
            curveScaleX = new AnimationCurve();
            curveScaleY = new AnimationCurve();
            curveScaleZ = new AnimationCurve();	
        }

        public void AddKeys (  float t ,   BoneTransform values   ){

            // Position x,y,z
            curvePosX.AddKey(new Keyframe(t,values.Values[0]));
            curvePosY.AddKey(new Keyframe(t,values.Values[1]));
            curvePosZ.AddKey(new Keyframe(t,values.Values[2]));
		
            // Rotation x,y,z,w
            curveRotX.AddKey(new Keyframe(t,values.Values[3]));
            curveRotY.AddKey(new Keyframe(t,values.Values[4]));
            curveRotZ.AddKey(new Keyframe(t,values.Values[5]));
            curveRotW.AddKey(new Keyframe(t,values.Values[6]));
		
            // Scale x,y,z
            curveScaleX.AddKey(new Keyframe(t,values.Values[7]));
            curveScaleY.AddKey(new Keyframe(t,values.Values[8]));
            curveScaleZ.AddKey(new Keyframe(t,values.Values[9]));		
        }
	
        public void PostAddKeys (  string objectRelativePath   ){
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localPosition.x", curvePosX);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localPosition.y", curvePosY);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localPosition.z", curvePosZ);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localRotation.x", curveRotX);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localRotation.y", curveRotY);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localRotation.z", curveRotZ);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localRotation.w", curveRotW);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localScale.x", curveScaleX);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localScale.y", curveScaleY);
            animclip.SetCurve(objectRelativePath, typeof(Transform), "localScale.z", curveScaleZ);	
        }
    }*/
    // The facefx controller is an immediate child of the player GameObject.  It holds per-animation information like the audio start time 
    // and curves for bone poses.  The audio start time is stored in the localPosition.x property of the facefx controller.  The controller 
    // has one child per bone pose, and the bone pose curves are stored in the localPosition.x property of the children.  The bone poses 
    // themselves are stored as Unity animations with the "facefx" prefix.  They drive the skeleton and are blended additively.  This 
    // object is created by the FaceFXImportXMLActor Editor script.
    private GameObject facefx_controller;



    public FaceFXControllerScript _target;
    void OnEnable()
    {
        _target = (FaceFXControllerScript)target;
    }
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Print Instructions To Log"))
        {
            string instructions = "The FaceFXControllerScript plays FaceFX animations on your Unity character.  " +
            "Attach the script to your character, then import a FaceFX XML Actor file that contains bone poses " +
            "and animation data.  Then play animations using the FaceFxControllerScript PlayAnim function. " +
            "\n\nTips:\n------------------------------------------" +
            "\n\nScaleFactor:\nChange the Scale Factor to match your FBX import settings.  Then Import the XML Actor file." +
            "\n\nFBX Ref Pose:\nCheck this box if your FBX file has the character in the reference pose.  It allows you to use " +
            "the same XML file on multiple characters even if their reference pose is slightly different (because one character " +
            "is taller for example).  The bone poses in the XML file will be transferred onto your character.  Be sure to import " +
            "your XML file after changing this setting." +
            "\n\nNo Face Graph Links:\nYour XML file's Face Graph should contain only bone poses, no links or combiner nodes because " +
            "the Unity integration can not evaluate the Face Graph.  Use the fgcollapse command from FaceFX Studio " +
            "prior to XML export to remove links and combiner nodes.  If you are analyzing audio from a plugin, use " +
            "the Assets\\PhraseLockedDemo\\Analysis-Actor-For-Unity-Demo-Plugin-Use.facefx file to get head " +
            "and eye rotations.\n\n";
            Debug.Log(instructions);
        }
        EditorGUILayout.BeginVertical("box");
        GUILayout.Label("Import Actor Settings: (applied when importing actor)");
        float scaleFactor = EditorGUILayout.FloatField("Scale Factor", _target.ScaleFactor);
        bool useFBXRefPose = EditorGUILayout.Toggle("FBX Ref Pose", _target.UseReferencePoseFromFBX);
        EditorGUILayout.EndVertical();
        if (GUI.changed)
        {
            _target.ScaleFactor = scaleFactor;
            _target.UseReferencePoseFromFBX = useFBXRefPose;
            Debug.Log("Import XML Actor settings have changed.  Import an XML Actor to use the new settings.");
        }
        if (GUILayout.Button("Import Actor & Animations"))
        {
            string path = EditorUtility.OpenFilePanel("Import FaceFX XML Actor C#", "", "xml");
            if (path.Length != 0)
            {
                if (File.Exists(path))
                {
                    Debug.Log("Importing XML Actor: " + path);
                    string file_contents = File.OpenText(path).ReadToEnd();
                    _target.ImportXML(file_contents, true);
                }
            }
        }
        // Only allow importing animations if there is a facefx_controller object.
        if (_target.GetFaceFXControllerGameObject())
        {
            if (GUILayout.Button("Import Animations Only"))
            {
                Debug.Log("Importing animations from XML file.");
                string path = EditorUtility.OpenFilePanel("Import FaceFX XML Animations C#", "", "xml");
                if (path.Length != 0)
                {
                    if (File.Exists(path))
                    {
                        Debug.Log("Importing animations from XML file: " + path);
                        string file_contents = File.OpenText(path).ReadToEnd();
                        _target.ImportXMLAnimations(file_contents);
                    }
                }
            }
        }
    }

    
}