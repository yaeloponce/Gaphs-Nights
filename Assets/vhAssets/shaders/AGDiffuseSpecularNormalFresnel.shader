Shader "AG/Diffuse Specular Normal Fresnel" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1)
        _SpecColor ("Spec Color", Color) = (1,1,1)
        _Shininess ("Shininess", Range (0.01,100)) = 0.01
        _FresnelColor ("Fresnel Color", Color) = (1,1,1)
        _FresnelStrength ("Fresnel Strength", Float) = 0.5
        _FresnelSpread ("Fresnel Spread", Float) = 0.5

        _MainTex ("Diffuse (RGB)", 2D) = "white" {}
        _SpecMap ("Specular Map (Grayscale)", 2D) = "white" {}
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _FresMap ("Fresnel Map (RGB)", 2D) = "white" {}
    }

    SubShader {
        Tags {"Queue"="Geometry" "IgnoreProjector"="True" "RenderType"="Opaque"}
        LOD 400

        CGPROGRAM
        #pragma target 3.0
        #pragma surface surf BlinnPhong
        #pragma exclude_renderers flash

        sampler2D _MainTex;
        sampler2D _SpecMap;
        sampler2D _BumpMap;
        sampler2D _FresMap;
        float3 _Color;
        float _Shininess;
        float4 _FresnelColor;
        float _FresnelStrength;
        float _FresnelSpread;

        struct Input {
            float2 uv_MainTex;
            float3 viewDir;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            float4 tex = tex2D(_MainTex, IN.uv_MainTex);
            float4 spc = tex2D(_SpecMap, IN.uv_MainTex);
            float4 frn = tex2D(_FresMap, IN.uv_MainTex);

            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));

            //The clamping gets rid of rendering artifacts when we approach the extreme viewDirs
            half fresnelFactor = clamp(dot(normalize(IN.viewDir), o.Normal), 0.1, 1);
            o.Emission.rgb = frn.rgb * _FresnelColor.rgb * (_FresnelStrength - fresnelFactor * _FresnelStrength);
            o.Emission.rgb = pow(o.Emission.rgb, (_FresnelSpread * fresnelFactor)) * _FresnelStrength;

            o.Albedo = tex.rgb * _Color;
            o.Gloss = spc.rgb;
            o.Specular = _Shininess * spc.rgb;

        }
    ENDCG
    }

FallBack "Bumped Diffuse"
}
