﻿using UnityEngine;
using System.Collections;

public class GestureUtils : MonoBehaviour
{
    #region Constants
    public enum Handedness
    {
        LEFT_HAND,
        RIGHT_HAND,
        BOTH_HANDS
    }

    public enum Emotion
    {
        angry,
        neutral,
        sad
    }

    public enum Style
    {
        energetic,

    }
    #endregion

    #region Variables

    #endregion

    #region Functions

    #endregion
}
