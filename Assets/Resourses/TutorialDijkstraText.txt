5
1) Tu misión es encontrar el camino más corto de la torre 0 a la torre 4.
2) Para encontrar el camino más corto desde el VERTICE ORIGEN a los demás VERTICES, necesitas calcular dicho camino a cada uno de ellos.
3) Si existe un camino entre el VERTICE actual (vertice padre, p[v]) se coloca la DISTANCIA de dicho camino (d[v]).
4) Si no existe un camino, se coloca una distancia (d[v]) INFINITA entre los VERTICEs.
5) Arrastra los nodos (p[v]) y las aristas (d[v]) a sus columnas correspondientes.